package scripts.fc.missions.fccooksassistant.tasks;

import org.tribot.api.Timing;
import org.tribot.api2007.Banking;
import org.tribot.api2007.Inventory;
import org.tribot.api2007.WebWalking;

import scripts.fc.api.generic.FCConditions;
import scripts.fc.framework.task.Task;
import scripts.fc.missions.fccooksassistant.data.QuestSettings;

public class InventoryCheck extends Task
{

	@Override
	public void execute()
	{
		if(Banking.isInBank())
		{
			if(Banking.isBankScreenOpen())
			{
				final int INV_SIZE = Inventory.getAll().length;
				if(Banking.depositAll() > 0)
					Timing.waitCondition(FCConditions.inventoryChanged(INV_SIZE), 4000);
			}
			else
				if(Banking.openBank())
					Timing.waitCondition(FCConditions.BANK_LOADED_CONDITION, 5000);
		}
		else
			WebWalking.walkToBank();
	}

	@Override
	public boolean shouldExecute()
	{
		return !QuestSettings.SPACE_BOOL.validate();
	}

	@Override
	public String getStatus()
	{
		return "Inventory check";
	}

}
