package scripts.fc.missions.fccooksassistant;

import java.util.Arrays;
import java.util.LinkedList;

import org.tribot.api.interfaces.Positionable;
import org.tribot.api2007.types.RSTile;

import scripts.fc.framework.goal.GoalList;
import scripts.fc.framework.mission.Mission;
import scripts.fc.framework.mission.MissionManager;
import scripts.fc.framework.script.FCScript;
import scripts.fc.framework.task.Task;
import scripts.fc.missions.fccooksassistant.data.QuestSettings;
import scripts.fc.missions.fccooksassistant.tasks.CollectFlour;
import scripts.fc.missions.fccooksassistant.tasks.CookDialogue;
import scripts.fc.missions.fccooksassistant.tasks.GetBucket;
import scripts.fc.missions.fccooksassistant.tasks.GetEgg;
import scripts.fc.missions.fccooksassistant.tasks.GetPot;
import scripts.fc.missions.fccooksassistant.tasks.InventoryCheck;
import scripts.fc.missions.fccooksassistant.tasks.MilkCow;
import scripts.fc.missions.fccooksassistant.tasks.MillWheat;
import scripts.fc.missions.fccooksassistant.tasks.PickWheat;

public class FCCooksAssistant extends MissionManager implements Mission
{
	public static final Positionable KITCHEN_TILE = new RSTile(3207, 3214, 0);
	
	public FCCooksAssistant(FCScript script)
	{
		super(script);
	}
	
	@Override
	public boolean hasReachedEndingCondition()
	{
		return QuestSettings.QUEST_COMPLETE.isValid();
	}

	@Override
	public String getMissionName()
	{
		return "Cook's Assistant";
	}

	@Override
	public String getCurrentTaskName()
	{
		return currentTask == null ? "null" : currentTask.getStatus();
	}

	@Override
	public String getEndingMessage()
	{
		return "Cook's Assistant has been completed.";
	}

	@Override
	public void execute()
	{
		executeTasks();
	}

	@Override
	public LinkedList<Task> getTaskList()
	{
		return new LinkedList<Task>(Arrays.asList(new InventoryCheck(), new GetPot(),
				new GetBucket(), new CookDialogue(), new MilkCow(), new GetEgg(), new PickWheat(),
				new MillWheat(), new CollectFlour()));
	}

	@Override
	public GoalList getGoals()
	{
		return goals;
	}
	
	public String toString()
	{
		return getMissionName();
	}

	@Override
	public String[] getMissionSpecificPaint()
	{
		return new String[0];
	}
}
