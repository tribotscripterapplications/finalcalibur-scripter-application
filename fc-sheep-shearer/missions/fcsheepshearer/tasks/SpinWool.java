package scripts.fc.missions.fcsheepshearer.tasks;

import org.tribot.api.Clicking;
import org.tribot.api.General;
import org.tribot.api.Timing;
import org.tribot.api.input.Keyboard;
import org.tribot.api.interfaces.Positionable;
import org.tribot.api2007.Combat;
import org.tribot.api2007.Game;
import org.tribot.api2007.GameTab;
import org.tribot.api2007.GameTab.TABS;
import org.tribot.api2007.Interfaces;
import org.tribot.api2007.Inventory;
import org.tribot.api2007.Player;
import org.tribot.api2007.WebWalking;
import org.tribot.api2007.types.RSArea;
import org.tribot.api2007.types.RSInterface;
import org.tribot.api2007.types.RSTile;

import scripts.fc.api.generic.FCConditions;
import scripts.fc.api.interaction.impl.objects.ClickObject;
import scripts.fc.framework.task.Task;
import scripts.fc.missions.fcsheepshearer.FCSheepShearer;
import scripts.fc.missions.fcsheepshearer.data.QuestStage;

public class SpinWool extends Task
{
	private final Positionable WHEEL_TILE = new RSTile(3209, 3213, 1);
	private final int INTERFACE_MASTER = 459;
	private final int INTERFACE_CHILD = 100;
	private final int ANIMATION_ID = 894;
	private final long ANIMATION_TIMEOUT = 2000;
	private final RSArea LUMBRIDGE_SPAWN = new RSArea(new RSTile(3221, 3218, 0), 10);
	
	private long lastAnimation;
	
	@Override
	public void execute()
	{
		if(!Combat.isUnderAttack() && ShearSheep.SHEEP_PEN.contains(Player.getPosition()))
			attemptTeleport();
		else if(Player.getPosition().distanceTo(WHEEL_TILE) > 1)
			WebWalking.walkTo(WHEEL_TILE);
		else
			spinWool();
	}

	@Override
	public boolean shouldExecute()
	{
		return Game.getSetting(FCSheepShearer.QUEST_SETTING_INDEX) == QuestStage.STARTED.getSetting()
				&& Inventory.getCount("Ball of wool") + Inventory.getCount("Wool") >= 20 &&
				Inventory.getCount("Ball of wool") < 20;
	}

	@Override
	public String getStatus()
	{
		return "Spin wool";
	}
	
	private void attemptTeleport()
	{
		final int MAGIC_MASTER = 218;
		final int HOME_TELE = 1;
		
		if(GameTab.open(TABS.MAGIC))
		{
			RSInterface teleInter = Interfaces.get(MAGIC_MASTER, HOME_TELE);
			
			if(teleInter != null && !teleInter.isHidden())
			{
				if(Clicking.click(teleInter) && Timing.waitCondition(FCConditions.animationChanged(-1), 2000))
				{
					Timing.waitCondition(FCConditions.inAreaCondition(LUMBRIDGE_SPAWN), 15000);
				}
				else
					WebWalking.walkTo(WHEEL_TILE);
			}
		}
	}
	
	private void spinWool()
	{
		if(Player.getAnimation() == ANIMATION_ID)
			lastAnimation = Timing.currentTimeMillis();
		else if(Timing.timeFromMark(lastAnimation) > ANIMATION_TIMEOUT)
		{
			RSInterface inter = Interfaces.get(INTERFACE_MASTER, INTERFACE_CHILD);
			
			if(inter == null || inter.isHidden())
			{
				if(new ClickObject("Spin", "Spinning wheel", 5).execute())
					Timing.waitCondition(FCConditions.interfaceUp(INTERFACE_MASTER), 3000);
			}
			else if(inter.click("Make X") && Timing.waitCondition(FCConditions.ENTER_AMOUNT_CONDITION, 2000))
			{
				Keyboard.typeString(""+Inventory.getCount("Wool"));
				General.sleep(300, 800);
				Keyboard.pressEnter();
				Timing.waitCondition(FCConditions.animationChanged(-1), 2000);
			}
		}
	}

}
