package scripts.fc.framework.requirement.impl;

import java.util.List;

import org.tribot.api2007.Skills.SKILLS;

import scripts.fc.api.skills.GatheringMode;
import scripts.fc.api.skills.ProgressionType;
import scripts.fc.api.skills.mining.data.RockType;
import scripts.fc.api.skills.mining.data.locations.impl.RimmingtonMine;
import scripts.fc.framework.goal.impl.ResourceGoal;
import scripts.fc.framework.goal.impl.SkillGoal;
import scripts.fc.framework.mission.Mission;
import scripts.fc.framework.requirement.item.ItemRequirement;
import scripts.fc.framework.requirement.item.ReqItem;
import scripts.fc.framework.script.FCScript;
import scripts.fc.missions.fcmining.FCMining;

public class DoricsQuestRequirement extends ItemRequirement
{
	public DoricsQuestRequirement(FCScript script)
	{
		super(script);
	}

	@Override
	public ReqItem[] getReqItems()
	{
		return new ReqItem[]
		{
			new ReqItem(RockType.CLAY.getItemId(), 6, 
					new FCMining(script, true, RockType.CLAY, GatheringMode.BANK, new RimmingtonMine(), ProgressionType.EXPERIENCE, new ResourceGoal(6, RockType.CLAY.getItemId()))), 
			
			new ReqItem(RockType.COPPER.getItemId(), 4, 
					new FCMining(script, true, RockType.COPPER, GatheringMode.BANK, new RimmingtonMine(), ProgressionType.EXPERIENCE, new ResourceGoal(4, RockType.COPPER.getItemId()))), 
			
			new ReqItem(RockType.IRON.getItemId(), 2,
					new FCMining(script, true, RockType.IRON, GatheringMode.BANK, new RimmingtonMine(), ProgressionType.EXPERIENCE, new ResourceGoal(2, RockType.IRON.getItemId())),
					new FCMining(script, true, null, GatheringMode.DROP_INVENTORY, new RimmingtonMine(), ProgressionType.EXPERIENCE, new SkillGoal(SKILLS.MINING, 15)))
		};
	}

	@Override
	public List<Mission> getReqMissions()
	{
		return null;
	}

}
