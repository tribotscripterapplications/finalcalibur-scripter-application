package scripts.fc.api.interaction;

import org.tribot.api.interfaces.Positionable;
import org.tribot.api2007.Camera;
import org.tribot.api2007.Player;
import org.tribot.api2007.Walking;

public abstract class EntityInteraction
{
	private final int DISTANCE_THRESHOLD = 7;
	
	protected String name;
	protected int id;
	protected String action;
	protected int searchDistance;
	protected Positionable position;
	
	public EntityInteraction(String action, String name, int searchDistance)
	{
		this.action = action;
		this.name = name;
		this.searchDistance = searchDistance;
	}
	
	public EntityInteraction(String action, int id, int searchDistance)
	{
		this.action = action;
		this.id = id;
		this.searchDistance = searchDistance;
	}
	
	public EntityInteraction(String action, Positionable position)
	{
		this.action = action;
		this.position = position;
	}
	
	public boolean execute()
	{
		if(position == null)
			findEntity();
		if(position != null)
		{
			prepareInteraction();
			return interact();
		}
		
		return false;
	}
	
	protected abstract boolean interact();
	
	protected abstract void findEntity();
	
	protected void prepareInteraction()
	{			
		if(Player.getPosition().distanceTo(position) >= DISTANCE_THRESHOLD)
			walkToPosition();
		
		if(!position.getPosition().isOnScreen())
		{
			Camera.turnToTile(position);
			
			if(!position.getPosition().isOnScreen())
				walkToPosition();
		}
	}
	
	private void walkToPosition()
	{
		Walking.blindWalkTo(position);		
	}
	
}
