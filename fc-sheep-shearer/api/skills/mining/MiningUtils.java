package scripts.fc.api.skills.mining;

import org.tribot.api.types.generic.Filter;
import org.tribot.api2007.Banking;
import org.tribot.api2007.Equipment;
import org.tribot.api2007.Inventory;
import org.tribot.api2007.Skills;
import org.tribot.api2007.Skills.SKILLS;
import org.tribot.api2007.ext.Filters;
import org.tribot.api2007.types.RSObject;

import scripts.fc.api.skills.mining.data.Pickaxe;
import scripts.fc.api.skills.mining.data.RockType;

public class MiningUtils
{
	public static Pickaxe getBestUsablePick(boolean isCheckingBank)
	{
		Pickaxe[] picks = Pickaxe.values();
		
		for(int i = picks.length - 1; i >= 0; i--)
		{
			Pickaxe pick = picks[i];
			if(pick.getMiningLevel() <= Skills.getActualLevel(SKILLS.MINING) //Mining level check
					&& ((Inventory.getCount(pick.getItemId()) > 0 || Equipment.getCount(pick.getItemId()) > 0) //Inventory check
							|| (isCheckingBank && Banking.find(pick.getItemId()).length > 0))) //Bank check
				return pick;
		}
		
		return null;
	}
	
	public static Pickaxe currentAppropriatePick()
	{
		Pickaxe[] picks = Pickaxe.values();
		
		for(int i = picks.length - 1; i >= 0; i--)
			if(picks[i].getMiningLevel() <= Skills.getActualLevel(SKILLS.MINING))
				return picks[i];
		
		return Pickaxe.IRON;
	}
	
	public static Filter<RSObject> rockFilter(RockType rock)
	{
		return new Filter<RSObject>()
		{
			@Override
			public boolean accept(RSObject o)
			{
				if(rock.getIds().contains(o.getID()))
				{
					return true;
				}
				else if(isColorMatch(o, rock))
				{
					rock.getIds().add(o.getID());
					
					return true;
				}
				
				return false;
			}
			
		}.combine(Filters.Objects.nameEquals("Rocks"), true);
	}
	
	private static boolean isColorMatch(RSObject o, RockType rock)
	{
		for(short color : o.getDefinition().getModifiedColors())
		{
			for(int i : rock.getColors())
			{
				if(color == i)
					return true;
			}
		}
		
		return false;
	}
}
