package scripts.fc.missions.fcmining.tasks;

import org.tribot.api.General;
import org.tribot.api2007.Player;
import org.tribot.api2007.Players;
import org.tribot.api2007.WorldHopper;
import org.tribot.api2007.ext.Filters;

import scripts.fc.api.utils.Utils;
import scripts.fc.api.worldhopping.FCInGameHopper;
import scripts.fc.framework.paint.FCPaint;
import scripts.fc.framework.task.Task;
import scripts.fc.missions.fcmining.FCMining;

public class WorldHop extends Task
{
	private static final long serialVersionUID = 343528012996773996L;
	
	private final long MIN_TIME_RAN = 60000 * 3;
	
	private FCMining script;
	private boolean isMember;
	private boolean playersInArea;
	private boolean stolenPerHour;
	private boolean noOreAvailable;
	
	public WorldHop(FCMining script)
	{
		this.script = script;
		this.isMember = Utils.isMember();
	}
	
	@Override
	public void execute()
	{
		General.println("Hopping worlds!");
		
		if(playersInArea)
			General.println("Too many players in area!");
		
		if(stolenPerHour)
			General.println("Too many ore stolen per hour!");
		
		if(noOreAvailable)
			General.println("No ore available!");
		
		script.oreStolen = 0;
		FCInGameHopper.hop(WorldHopper.getRandomWorld(isMember, false));
	}

	@Override
	public boolean shouldExecute()
	{
		if(!script.location.getArea().contains(Player.getPosition()))
			return false;
		
		FCPaint detailedPaint = script.fcScript.paint;
		
		playersInArea = script.hopSettings.playersInArea > 0 &&
										(Players.getAll(Filters.Players.inArea(script.location.getArea())).length - 1) >= script.hopSettings.playersInArea;
										
		stolenPerHour = script.hopSettings.resourceStolenPerHour > 0 && detailedPaint.getTimeRanMs() >= MIN_TIME_RAN && 
										detailedPaint.getPerHour(script.oreStolen) >= script.hopSettings.resourceStolenPerHour;
		
		noOreAvailable = script.hopSettings.noResourceAvailable && script.ROCK_FINDER.getRocks().length == 0;
		
		return playersInArea || stolenPerHour || noOreAvailable;
	}

	@Override
	public String getStatus()
	{
		return "World hop";
	}

}
